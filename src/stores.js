import { writable } from 'svelte/store';

export const lightboxTrigger = writable({
  lightboxIsDisplayed: false,
  type: "modal",
  heading: "",
  content: "",
  ctas: [],
  footnote: ""
});
