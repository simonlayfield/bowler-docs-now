var menu = new MainMenu({
  target: document.querySelector('#main-menu'),
  data: {
    sections: [{
      name: 'Core',
      items: [{
          name: 'Colours',
          link: '/core/colours'
        }, {
          name: 'Typography',
          link: '/core/typography'
        }, {
          name: 'Technologies',
          link: '/guides/technologies'
        }]
    }, {
      name: 'Components',
      items: [{
          name: 'Menus',
          link: '/components/menus'
        }, {
          name: 'Buttons',
          link: '/components/buttons'
        }, {
          name: 'Bars',
          link: '/components/bars'
        }, {
          name: 'Lightbox',
          link: '/components/lightbox'
        }, {
          name: 'Pricing Card',
          link: '/components/pricing-card'
        }, {
          name: 'Countdown',
          link: '/components/countdown'
        }, {
          name: 'Banners',
          link: '/components/banners'
        }]
    }, {
      name: 'Design',
      items: [{
        name: 'EDM',
        link: '/design/edm'
      }]
    }, {
      "name": "Guides",
      "items": [{
          "name": "Create a site",
          "link": "/guides/creating-a-site"
        }]
    }, {
      "name": "Info",
      "items": [{
          "name": "About Bowler",
          "link": "/about"
        }]
    }]
  }
});
