import Home from '../src/views/Home.svelte';
import Docs from '../src/views/Docs.svelte';
import About from '../src/views/About.svelte';
import Colours from '../src/views/core/Colours.svelte';
import Typography from '../src/views/core/Typography.svelte';
import Bar from '../src/views/components/Bar.svelte';
import Button from '../src/views/components/Button.svelte';
import Banner from '../src/views/components/Banner.svelte';
import Countdown from '../src/views/components/Countdown.svelte';
import Menu from '../src/views/components/Menu.svelte';
import PricingCard from '../src/views/components/PricingCard.svelte';
import Lightbox from '../src/views/components/Lightbox.svelte';
import Technologies from '../src/views/core/Technologies.svelte';
import Edm from '../src/views/design/Edm.svelte';
import NotFound from '../src/views/NotFound.svelte';

let routes = {
  '/': Home,
  '/docs': Docs,
  '/about': About,
  '/core/colours': Colours,
  '/core/typography': Typography,
  '/core/technologies': Technologies,
  '/components/bar': Bar,
  '/components/button': Button,
  '/components/banner': Banner,
  '/components/countdown': Countdown,
  '/components/menu': Menu,
  '/components/pricingcard': PricingCard,
  '/components/lightbox': Lightbox,
  '/design/edm': Edm,
  // Catch-all, must be last
  '*': NotFound
}

export default routes;
